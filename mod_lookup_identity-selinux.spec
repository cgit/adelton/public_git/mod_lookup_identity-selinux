
%define selinux_variants mls strict targeted
%define selinux_policyver %(sed -e 's,.*selinux-policy-\\([^/]*\\)/.*,\\1,' /usr/share/selinux/devel/policyhelp 2> /dev/null)
%define modulename mod_lookup_identity

Name:		mod_lookup_identity-selinux
Version:	1.0
Release:	1%{?dist}
Summary:	SELinux module to allow httpd to talk to sssd via dbus

Group:		System Environment/Base
License:	GPLv2+
URL:		http://fedorapeople.org/cgit/adelton/public_git/%{name}.git/
Source0:	http://fedorapeople.org/cgit/adelton/public_git/%{name}.git/snapshot/%{name}-%{version}.tar.gz

BuildRequires:	make, selinux-policy-devel
BuildRequires:	policycoreutils >= %{POLICYCOREUTILSVER}
BuildArch:	noarch


%if "%{selinux_policyver}" != ""
Requires:	selinux-policy >= %{selinux_policyver}
%endif

Requires(post):		/usr/sbin/semodule
Requires(postun):	/usr/sbin/semodule

%description
SELinux module to allow httpd to talk to sssd via dbus, to support
mod_lookup_identity on systems that do not (yet) have SELinux
boolean httpd_dbus_sssd.

%prep
%setup -q

%build

for selinuxvariant in %{selinux_variants}; do
	make NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile
	mv %{modulename}.pp %{modulename}.pp.${selinuxvariant}
	make NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile clean
done

%install
rm -rf %{buildroot}/*
for selinuxvariant in %{selinux_variants} ; do
	install -d %{buildroot}%{_datadir}/selinux/${selinuxvariant}
	install -p -m 644 %{modulename}.pp.${selinuxvariant} %{buildroot}%{_datadir}/selinux/${selinuxvariant}/%{modulename}.pp
done

%post
for selinuxvariant in %{selinux_variants} ; do
	/usr/sbin/semodule -s ${selinuxvariant} -l > /dev/null 2>&1 \
		&& /usr/sbin/semodule -s ${selinuxvariant} -i /usr/share/selinux/${selinuxvariant}/%{modulename}.pp || :
done

%postun
# Clean up after package removal
if [ $1 -eq 0 ]; then
	for selinuxvariant in %{selinux_variants} ; do
		/usr/sbin/semodule -s ${selinuxvariant} -l > /dev/null 2>&1 \
			&& /usr/sbin/semodule -s ${selinuxvariant} -r %{modulename} || :
	done
fi

%files
%doc %{modulename}.te
%{_datadir}/selinux/*/%{modulename}.pp

%changelog
* Wed Jun 25 2014 Jan Pazdziora 1.0-1
- Initial release.

